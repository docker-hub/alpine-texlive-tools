FROM registry.gitlab.com/docker-hub/alpine-texlive:latest

RUN apk add --no-cache \
      # inkscape (needed for images)
      inkscape \
      # Git and CA certificates
      git \
      ca-certificates \
      # gnuplot (for pgfplots advance settings)
      gnuplot \
      # easy way to unzip (also one can tar)
      unzip
